import { Rule, SchematicContext, Tree, externalSchematic, url, apply, mergeWith, MergeStrategy, chain, template } from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';

// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function newRepo(_options: any): Rule {

  const name = _options.name;

  console.log('Hello from your new schematic!');
  console.log('Here your options:');
  console.log(_options);

  return (tree: Tree, _context: SchematicContext) => {

    const templateSource = apply(url('./template-files'), [
      template({..._options, ...strings}),
    ]);

    const rule = chain([
      generateRepo(name),
      mergeWith(templateSource, MergeStrategy.Overwrite),
      updatePackageJson(name)
    ]);

    return rule(tree, _context) as Rule;
  }
}

  
function generateRepo(name: string): Rule {
  return (_: Tree, _context: SchematicContext) => {
    return externalSchematic('@schematics/angular', 'ng-new', {
      name,
      version: '9.0.0',
      directory: name,
      routing: true,
      style: 'scss',
      inlineStyle: false,
      inlineTemplate: false
    });
  };
}

function updatePackageJson(name: string): Rule {
  return (tree: Tree): Tree => {
    const path = `/${name}/package.json`;
    const file = tree.read(path);
    const json = JSON.parse(file!.toString());

    json.scripts = {
      ...json.scripts,
      'build:prod': 'ng build --prod',
      'test:ci': 'ng test --no-watch --code-coverage'
    };
      
    json.husky = {
      'hooks': {
        'pre-commit': 'pretty-quick --staged --pattern \"apps/**/**/*.{ts,scss,html}\"'
      }
    };
      
    json.devDependencies.prettier = '^2.0.0';
    json.devDependencies.husky = '^4.2.0';

    tree.overwrite(path, JSON.stringify(json, null, 2));
    return tree;
  }
 }